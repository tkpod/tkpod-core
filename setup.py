from setuptools import setup


def read_md(file):
    with open(file) as fin:
        return fin.read()


setup(
    name="tkpod-core",
    version="0.1.11",
    description="Core libraries for the `tkpod` project.",
    long_description=read_md("README.md"),
    author="Alexey Strokach",
    author_email="alex.strokach@utoronto.ca",
    url="https://gitlab.com/tkpod/tkpod-core",
    packages=["tkpod.core"],
    package_data={},
    include_package_data=True,
    zip_safe=False,
    keywords="tkpod",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ],
    test_suite="tests",
)
