import logging
import sys

import fire

from tkpod.core.interface import Tool
from tkpod.core.utils import find_leaf_subclasses, find_plugins

logging.getLogger("kmbio.PDB.core.atom").setLevel(logging.WARNING)


def main():
    logging.basicConfig(format="[%(levelname)s]: %(message)s", level=logging.DEBUG)
    find_plugins()
    tools = {cls.__name__: cls for cls in find_leaf_subclasses(Tool)}
    fire.Fire(tools)


if __name__ == "__main__":
    sys.exit(main())
