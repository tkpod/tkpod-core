# tkpod-core

[![conda](https://img.shields.io/conda/dn/tkpod/tkpod-core.svg)](https://anaconda.org/tkpod/tkpod-core/)
[![docs](https://img.shields.io/badge/docs-vv0.1.11-blue.svg)](https://tkpod.gitlab.io/tkpod-core/v0.1.11/)
[![build status](https://gitlab.com/tkpod/tkpod-core/badges/vv0.1.11/build.svg)](https://gitlab.com/tkpod/tkpod-core/commits/v0.1.11/)
[![coverage report](https://gitlab.com/tkpod/tkpod-core/badges/vv0.1.11/coverage.svg)](https://tkpod.gitlab.io/tkpod-core/v0.1.11/htmlcov/)

Core libraries for the `tkpod` project.
