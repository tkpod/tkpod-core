import pytest

import tkpod.core


@pytest.mark.parametrize("attribute", ["__version__"])
def test_attribute(attribute):
    assert getattr(tkpod.core, attribute)


def test_main():
    import tkpod.core

    assert tkpod.core
