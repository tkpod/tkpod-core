import pytest

from tkpod.core.types import Mutation


@pytest.mark.parametrize("mutation_str, mutation_obj_", [("M1A", Mutation("", "M", 0, "A"))])
def test_mutation_from_string(mutation_str, mutation_obj_):
    mutation_obj = Mutation.from_string(mutation_str)
    assert mutation_obj == mutation_obj_
