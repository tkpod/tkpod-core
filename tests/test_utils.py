import os.path as op

import kmbio.PDB
import numpy as np
import pytest

from tkpod.core.utils import (
    _choose_closest_chain,
    extract_ligand_and_target,
    get_min_distance,
    is_hetatm,
)

TESTS_DIR = op.dirname(op.abspath(__file__))


@pytest.mark.parametrize(
    "structure_file, polymer_chains_, hetatm_chains_",
    [
        #
        ("1brs.cif", ["A", "D"], ["G", "J"])
    ],
)
def test_is_hetatm(structure_file, polymer_chains_, hetatm_chains_):
    structure = kmbio.PDB.load(
        op.join(TESTS_DIR, "structures", structure_file), bioassembly_id=1, use_auth_id=False
    )
    polymer_chains = sorted(c.id for c in structure.chains if not is_hetatm(c))
    hetatm_chains = sorted(c.id for c in structure.chains if is_hetatm(c))
    assert polymer_chains == polymer_chains_
    assert hetatm_chains == hetatm_chains_


@pytest.mark.parametrize(
    "coord, target_array, distance_",
    [
        #
        ([0, 0, 0], [[1, 1, 1], [-2, -2, -2], [10, -10, 0]], 3 ** 0.5)
    ],
)
def test_get_min_distance(coord, target_array, distance_):
    distance = get_min_distance(np.array(coord), np.array(target_array))
    assert distance == distance_


@pytest.mark.skip(reason="kmbio.PDB.NeighborSearch has been removed.")
@pytest.mark.parametrize(
    "structure_file, atom_coords, closest_chain_id_",
    [
        #
        ("1brs.cif", [[0, 0, 0]], "A")
    ],
)
def test_choose_closest_chain(structure_file, atom_coords, closest_chain_id_):
    # Format residue
    residue = kmbio.PDB.Residue((" ", 1, " "), "ALA", 0)
    for i, coord in enumerate(atom_coords):
        atom = kmbio.PDB.Atom("C{}".format(i), np.array(coord), None, None, None, "xxxx", None, "C")
        residue.add(atom)
    # Format NSS and chains
    structure = kmbio.PDB.load(
        op.join(TESTS_DIR, "structures", structure_file), bioassembly_id=1, use_auth_id=False
    )
    nss = [kmbio.PDB.NeighborSearch(list(chain.atoms)) for chain in structure.chains]
    chains = list(structure.chains)
    # Check
    closest_chain = _choose_closest_chain(residue, nss, chains)
    assert closest_chain.id == closest_chain_id_


@pytest.mark.skip(reason="kmbio.PDB.NeighborSearch has been removed.")
@pytest.mark.parametrize(
    "structure_file, target_chain_id,"
    "chain_ids_ligand_, chain_ids_target_, chain_lengths_ligand_, chain_lengths_target_",
    [
        #
        ("1brs.cif", "D", ["A", "G", "J"], ["D", "G", "J"], [3, 108, 144], [1, 74, 87])
    ],
)
def test_extract_ligand_and_target(
    structure_file,
    target_chain_id,
    chain_ids_ligand_,
    chain_ids_target_,
    chain_lengths_ligand_,
    chain_lengths_target_,
):
    structure = kmbio.PDB.load(
        op.join(TESTS_DIR, "structures", structure_file), bioassembly_id=1, use_auth_id=False
    )
    structure_ligand, structure_target = extract_ligand_and_target(structure, target_chain_id)

    assert sorted(c.id for c in structure_ligand.chains) == chain_ids_ligand_
    assert sorted(c.id for c in structure_target.chains) == chain_ids_target_

    assert sorted(len(list(c)) for c in structure_ligand.chains) == chain_lengths_ligand_
    assert sorted(len(list(c)) for c in structure_target.chains) == chain_lengths_target_
